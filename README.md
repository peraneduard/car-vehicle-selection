# Exam 2

Vehicle Selection Form

## Prerequisites

- Installed [Node.js](https://nodejs.org/).

## Running the app

First, run the backend node server, the backend is split into sub-folder `backend`

```bash
$ cd backend
$ npm install
$ npm run dev
```

The frontend is using next JS.

```bash
$ npm install
$ npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
