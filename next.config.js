/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  rewrites: async () => {
    return [
      {
        source: "/api/v1/cars/:path*",
        destination: `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/v1/cars/:path*`,
      },
    ];
  },

  trailingSlash: true,
};

module.exports = nextConfig;
