import * as fs from "fs";
import { Car } from "../models";

const carsDataPath = "./data/cars.json";

export const getCarsJson = async (): Promise<Car[]> => {
  const json = await fs.promises.readFile(carsDataPath);
  const data = JSON.parse(json as any);
  return Object.values(data);
};

export interface SaveCarJson extends Car {
  filename?: string;
}

export const saveCarJson = async (data: SaveCarJson): Promise<void> => {
  let existCars = await getCarsJson();
  existCars.push(data);

  const stringifyData = JSON.stringify(
    existCars.reduce((d, c) => {
      d = { ...d, [c.id.toString()]: c };
      return d;
    }, {})
  );
  await fs.promises.writeFile(carsDataPath, stringifyData);
};

export const emptyCarJson = async (): Promise<void> => {
  await fs.promises.writeFile(carsDataPath, "{}");
};
