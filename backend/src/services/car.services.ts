import * as fs from "fs";
import { Car } from "../models";
import { getCarsJson, saveCarJson } from "../utils";

const getCars = async (id?: number): Promise<Car[] | Car | undefined> => {
  const cars = await getCarsJson();

  if (id) return cars.find((c) => c.id === id);

  return cars;
};

const addCar = async (params: {
  make: string;
  model: string;
  badge: string;
  serviceLogBookFile?: Express.Multer.File;
}): Promise<Car> => {
  const { serviceLogBookFile, ...rest } = params;
  const id = Math.floor(100000 + Math.random() * 900000);

  const logsText = await fs.promises.readFile(
    `uploads/${serviceLogBookFile?.filename}`
  );
  const serviceLogBook = logsText.toString();

  const car: Car = {
    ...rest,
    id,
    serviceLogBook,
  };

  await saveCarJson({ ...car, filename: serviceLogBookFile?.filename });

  return car;
};

export default { getCars, addCar };
