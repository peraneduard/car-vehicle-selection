export interface Car {
  id: Number;
  make: string;
  model: string;
  badge: string;
  serviceLogBook?: string;
}
