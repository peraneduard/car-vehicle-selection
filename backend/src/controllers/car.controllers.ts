import { Request, Response } from "express";
import carService from "../services/car.services";

export const getCarsController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const cars = await carService.getCars();
  res.status(200).json(cars);
};

export const getCarController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const car = await carService.getCars(+req.params.id);
  res.status(200).json(car);
};

export const addCarController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { make, model, badge } = req.body;
  const { files } = req;
  const serviceLogBookFile =
    Array.isArray(files) && files?.length ? files[0] : undefined;

  const car = await carService.addCar({
    make,
    model,
    badge,
    serviceLogBookFile,
  });

  res.status(200).json(car);
};
