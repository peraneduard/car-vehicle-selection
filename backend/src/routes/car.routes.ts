import { Router } from "express";
import {
  getCarsController,
  getCarController,
  addCarController,
} from "../controllers/car.controllers";
import { Multer } from "multer";

const multer = require("multer");
const router = Router();
const upload = multer({ dest: "uploads/" }) as Multer;

router
  .route("/")
  .get(getCarsController)
  .post(upload.array("files"), addCarController);

router.route("/:id").get(getCarController);

export { router };
