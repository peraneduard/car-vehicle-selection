import app from "./app";

require("dotenv").config();

const port: number = +(process.env.PORT as string) || 3001;

app.listen(port, (): void => console.log(`running on port ${port}`));
