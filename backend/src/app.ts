import express, { Application, Request, Response, NextFunction } from "express";
import * as bodyParser from "body-parser";
import { router as carRoutes } from "./routes/car.routes";
const cors = require("cors");

const app: Application = express();

// middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// route
app.use("/api/v1/cars", carRoutes);
app.use("/", (req: Request, res: Response, next: NextFunction): void => {
  res.json({ message: "Allo! Catch-all route." });
});

export default app;
