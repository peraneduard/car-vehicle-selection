export interface Car {
  id: number;
  make: string;
  model: string;
  badge: string;
  serviceLogBook?: string;
}
