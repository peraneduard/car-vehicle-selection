import { useEffect } from "react";
import { addCarApi, getCarApi } from "@/apis/car";
import { Car } from "@/models/car";
import { useState } from "react";

export interface SaveCarParams extends Car {
  serviceLogBookFiles?: File[];
  success?: (data: Car) => void;
  failed?: (e: Error) => void;
}

export interface UseAddCarResult {
  isLoading: boolean;
  error?: string;
  save: (params: SaveCarParams) => Promise<void>;
}

export const useAddCar = (): UseAddCarResult => {
  const [error, setError] = useState<string>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const save = async (params: SaveCarParams): Promise<void> => {
    const { serviceLogBookFiles, success, failed } = params;

    const formData = new FormData();
    formData.append("make", params.make);
    formData.append("model", params.model);
    formData.append("badge", params.badge);

    if (serviceLogBookFiles)
      for (let file of serviceLogBookFiles)
        formData.append("files", file, file.name);

    try {
      setIsLoading(true);
      const car = await addCarApi({ formData });
      setError("");

      if (typeof success === "function") success(car);
    } catch (ex) {
      setError("Error on record save car.");
      if (typeof failed === "function") failed(ex as Error);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    error,
    isLoading,
    save,
  };
};

export interface UseCarResult {
  car?: Car;
  isLoading: boolean;
  error?: string;
  refresh: () => Promise<void>;
}

export const useFetchCar = (carId: number): UseCarResult => {
  const [car, setCar] = useState<Car>();
  const [error, setError] = useState<string>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const getCar = async (): Promise<Car | undefined> => {
    try {
      setIsLoading(true);

      const car = await getCarApi(carId);

      setCar(car);
      setError("");

      return car;
    } catch (ex) {
      console.error(ex);

      setError("Error on fetch car.");
    } finally {
      setIsLoading(false);
    }
  };

  async function fetchData() {
    await getCar();
  }

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [carId]);

  return {
    car,
    error,
    isLoading,
    refresh: fetchData,
  };
};
