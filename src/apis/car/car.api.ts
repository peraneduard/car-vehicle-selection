import { Car } from "@/models/car";
import axios from "axios";

const request = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
});

export interface AddCarParams {
  formData: FormData;
  onUploadProgress?: (percentCompleted: number) => void;
}

export const addCarApi = async (params: AddCarParams): Promise<Car> => {
  const { formData, onUploadProgress } = params;

  const res = await request.post<Car>("/api/v1/cars", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
    onUploadProgress: (progressEvent) => {
      const percentCompleted = Math.round(
        (progressEvent.loaded * 100) / (progressEvent?.total ?? 0)
      );

      if (typeof onUploadProgress === "function")
        onUploadProgress(percentCompleted);
    },
  });

  return res.data;
};

export const getCarApi = async (carId: number): Promise<Car> => {
  const res = await request.get(`/api/v1/cars/${carId}`);

  return res.data;
};
