import { ReactElement } from "react";
import { useRouter } from "next/router";
import { useFetchCar } from "@/hooks/car.hooks";

export const Details = (): ReactElement => {
  const router = useRouter();
  const carId = router.query.id as any;

  const { car, error, isLoading, refresh } = useFetchCar(+carId);

  return (
    <>
      <div style={{ width: "100%", padding: "1rem" }}>
        <p>Make: {car?.make || ""}</p>
        <p>Model: {car?.model || ""}</p>
        <p>Badge: {car?.badge || ""}</p>
        <br />

        <p>Logbook:</p>
        <br />
        <pre>{car?.serviceLogBook || ""}</pre>
      </div>
    </>
  );
};

export default Details;
