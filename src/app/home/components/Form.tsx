import { ReactElement, useEffect, useMemo } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import Router from "next/router";
import { useAddCar } from "@/hooks/car.hooks";
import { Car } from "@/models/car";
import style from "./form.module.scss";

const CARS = {
  ford: {
    Ranger: ["Raptor", "Raptor x", "wildtrak"],
    Falcon: ["XR6", "XR6 Turbo", "XR8"],
    "Falcon Ute": ["XR6", "XR6 Turbo"],
  },
  bmw: {
    "130d": ["xDrive 26d", "xDrive 30d"],
    "240i": ["xDrive 30d", "xDrive, 50d"],
    "320e": ["xDrive 75d", "xDrive 80d", "xDrive 85d"],
  },
  tesla: {
    Model_3: ["Performance", "Long Range", "Dual Motor"],
  },
};

export interface CarForm extends Car {
  serviceLogBookFiles?: File[];
}

export const Form = (): ReactElement => {
  const { register, reset, watch, setValue, getValues, handleSubmit } =
    useForm<CarForm>();
  const make = watch("make");
  const model = watch("model");
  const { save } = useAddCar();

  const rediretTo = async (path: string): Promise<boolean> => {
    return await Router.push(new URL(path, location.origin));
  };

  const submitForm: SubmitHandler<CarForm> = async (data) => {
    await save({
      ...data,
      success: (data) => {
        reset();
        rediretTo(`/details/${data.id}`);
      },
    });
  };

  const modelOptions = useMemo(() => {
    switch (make) {
      case "ford":
        return CARS.ford;
      case "bmw":
        return CARS.bmw;
      case "tesla":
        return CARS.tesla;
      default:
        return {};
    }
  }, [make]) as any;

  const badgeOptions = useMemo(() => {
    if (!make || !model) return [];

    return modelOptions[model] || [];
  }, [make, modelOptions, model]);

  useEffect(() => {
    if (!Object.keys(modelOptions).includes(getValues("model")))
      setValue("model", "");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modelOptions]);

  useEffect(() => {
    if (!badgeOptions.includes(getValues("badge"))) setValue("badge", "");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [badgeOptions]);

  return (
    <>
      <form className={style.form} onSubmit={handleSubmit(submitForm)}>
        <div className={style.inputGroup}>
          <select {...register("make")}>
            <option value="make" selected disabled hidden>
              make
            </option>
            {Object.keys(CARS).map((m) => (
              <option key={m} value={m}>
                {m}
              </option>
            ))}
          </select>
        </div>
        <div className={style.inputGroup}>
          <select {...register("model")}>
            <option value="model" selected disabled hidden>
              model
            </option>
            {Object.keys(modelOptions).map((p) => (
              <option key={p} value={p}>
                {p}
              </option>
            ))}
          </select>
        </div>
        <div className={style.inputGroup}>
          <select {...register("badge")}>
            <option value="badge" selected disabled hidden>
              badge
            </option>
            {badgeOptions.map((b: string) => (
              <option key={b} value={b}>
                {b}
              </option>
            ))}
          </select>
        </div>
        <div className={style.inputGroup}>
          <input {...register("serviceLogBookFiles")} type="file"></input>
        </div>
        <div>
          <input type="submit" />
        </div>
      </form>

      <div>
        <h2>Select a Vehicle</h2>
        <button
          onClick={() => {
            setValue("make", "tesla");
            setValue("model", "Model_3");
            setValue("badge", "Performance");
          }}
        >
          Tesla Model 3 Performance
        </button>
        <br />
        <button
          onClick={() => {
            setValue("make", "bmw");
            setValue("model", "130d");
            setValue("badge", "xDrive 26d");
          }}
        >
          BMW 130d xDrive 26d
        </button>
      </div>
    </>
  );
};

export default Form;
